#!/bin/bash
###########################################################################
##                                                                       ##
##                   Language Technologies Institute                     ##
##                     Carnegie Mellon University                        ##
##                         Copyright (c) 2014                            ##
##                        All Rights Reserved.                           ##
##                                                                       ##
##  Permission is hereby granted, free of charge, to use and distribute  ##
##  this software and its documentation without restriction, including   ##
##  without limitation the rights to use, copy, modify, merge, publish,  ##
##  distribute, sublicense, and/or sell copies of this work, and to      ##
##  permit persons to whom this work is furnished to do so, subject to   ##
##  the following conditions:                                            ##
##   1. The code must retain the above copyright notice, this list of    ##
##      conditions and the following disclaimer.                         ##
##   2. Any modifications must be clearly marked as such.                ##
##   3. Original authors' names are not deleted.                         ##
##   4. The authors' names are not used to endorse or promote products   ##
##      derived from this software without specific prior written        ##
##      permission.                                                      ##
##                                                                       ##
##  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK         ##
##  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      ##
##  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   ##
##  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE      ##
##  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    ##
##  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   ##
##  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          ##
##  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       ##
##  THIS SOFTWARE.                                                       ##
##                                                                       ##
###########################################################################
##                                                                       ##
##  Computes the average prediction of the outputs of the trees in the   ##
##  random forest.                                                       ##
##                                                                       ##
###########################################################################

# Should be run in the rf_model directory. This script should never be 
# run on its own

statename=`basename $1 .feats`

echo "Computing average of the predictions for $statename"

CG_TMP=jnk_$$

ls -1d trees_?? > $CG_TMP.tree_list

num_trees=`wc -l $CG_TMP.tree_list | awk '{print $1}'`

tree1=`head -n 1 $CG_TMP.tree_list`

tree2=`head -n 2 $CG_TMP.tree_list | tail -n +2`

# First set of sums

$SPTKDIR/bin/x2x +af $tree1/$statename.wagon_encd > $CG_TMP.file1
$SPTKDIR/bin/x2x +af $tree2/$statename.wagon_encd > $CG_TMP.file2

$SPTKDIR/bin/vopr -a $CG_TMP.file1 $CG_TMP.file2 > $CG_TMP.accum

# Let's compute the rest now
tail -n +3 $CG_TMP.tree_list > $CG_TMP.rest_list

cat $CG_TMP.rest_list | 
while read tree_nm
do

    $SPTKDIR/bin/vopr -a $CG_TMP.accum $tree_nm/$statename.wagon_encd > $CG_TMP.temp_accum
    mv $CG_TMP.temp_accum $CG_TMP.accum

done


# Divide by number of trees to compute average
$SPTKDIR/bin/sopr -d $num_trees $CG_TMP.accum | $SPTKDIR/bin/x2x +fa57 > avg_preds/$statename.preds

# Create the dark feats and dark preds by concatenating the prediction of the
# RF and the original data (not in that order)
cat orig_data/$statename.feats $tree1/$statename.feats > dark_feats/$statename.feats
cat orig_data/$statename.wagon_encd avg_preds/$statename.preds | 
$ESTDIR/bin/ch_track -itype ascii -otype est_binary -s 0.005 -o dark_preds/$statename.preds 


rm -f $CG_TMP.*

