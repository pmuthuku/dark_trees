#!/bin/bash
###########################################################################
##                                                                       ##
##                   Language Technologies Institute                     ##
##                     Carnegie Mellon University                        ##
##                         Copyright (c) 2014                            ##
##                        All Rights Reserved.                           ##
##                                                                       ##
##  Permission is hereby granted, free of charge, to use and distribute  ##
##  this software and its documentation without restriction, including   ##
##  without limitation the rights to use, copy, modify, merge, publish,  ##
##  distribute, sublicense, and/or sell copies of this work, and to      ##
##  permit persons to whom this work is furnished to do so, subject to   ##
##  the following conditions:                                            ##
##   1. The code must retain the above copyright notice, this list of    ##
##      conditions and the following disclaimer.                         ##
##   2. Any modifications must be clearly marked as such.                ##
##   3. Original authors' names are not deleted.                         ##
##   4. The authors' names are not used to endorse or promote products   ##
##      derived from this software without specific prior written        ##
##      permission.                                                      ##
##                                                                       ##
##  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK         ##
##  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      ##
##  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   ##
##  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE      ##
##  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    ##
##  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   ##
##  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          ##
##  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       ##
##  THIS SOFTWARE.                                                       ##
##                                                                       ##
###########################################################################
##                                                                       ##
##  An attempt at Hinton's dark knowledge using Random Forests           ##
##                                                                       ##
###########################################################################

# This runs in rf_models/trees_xx/

fname=`basename $1 _mcep.tree`

CG_TMP=jnk_$$

echo "Getting MCEP predictions from $fname"

# Let's create another copy of the feats file for simplicity's sake
cp  ../../festival/feats/$fname.feats .

# Prod the trees to get information out
$ESTDIR/bin/wagon_test -tree $1 -desc ../../festival/clunits/mcep.desc -data $fname.feats -predict -heap $SIODHEAPSIZE -o $CG_TMP.wagon_encd

# Get rid of Standard Deviations (or are they variances?)
sed 's/[()]/ /g' $CG_TMP.wagon_encd | perl -ane '
    for ($i=0; $i <= ($#F-1); $i=$i+2){
         print "$F[$i] ";
    }
    print "\n";' > $fname.wagon_encd

    
rm -f $CG_TMP.*

