#!/bin/bash

# This is intended to be run within clustergen.scm and not separately

CG_TMP=jnk_$$

CODE_DIR=/home/pmuthuku/code/Dark_trees/

echo "-0.5 0 0.5" | x2x +af > $CG_TMP.delta

echo "Doing MLPG"

$ESTDIR/bin/ch_track -c 0 param_track -o $CG_TMP.f0

# Filtering out just the statics and deltas
$ESTDIR/bin/ch_track param_track -otype ascii | perl -lane 'print "@F[2..102]";' | perl $CODE_DIR/sptk_mlpg_prep.pl | $SPTKDIR/bin/x2x +af > $CG_TMP.pdf

# Do MLPG
$SPTKDIR/bin/mlpg -m 24 -d $CG_TMP.delta $CG_TMP.pdf | $SPTKDIR/bin/x2x +fa25  > $CG_TMP.mgc

paste $CG_TMP.f0 $CG_TMP.mgc | $ESTDIR/bin/ch_track -s 0.005 -itype ascii -otype est_ascii -o param1_track

rm -f $CG_TMP.*
