#!/bin/bash
###########################################################################
##                                                                       ##
##                   Language Technologies Institute                     ##
##                     Carnegie Mellon University                        ##
##                         Copyright (c) 2014                            ##
##                        All Rights Reserved.                           ##
##                                                                       ##
##  Permission is hereby granted, free of charge, to use and distribute  ##
##  this software and its documentation without restriction, including   ##
##  without limitation the rights to use, copy, modify, merge, publish,  ##
##  distribute, sublicense, and/or sell copies of this work, and to      ##
##  permit persons to whom this work is furnished to do so, subject to   ##
##  the following conditions:                                            ##
##   1. The code must retain the above copyright notice, this list of    ##
##      conditions and the following disclaimer.                         ##
##   2. Any modifications must be clearly marked as such.                ##
##   3. Original authors' names are not deleted.                         ##
##   4. The authors' names are not used to endorse or promote products   ##
##      derived from this software without specific prior written        ##
##      permission.                                                      ##
##                                                                       ##
##  CARNEGIE MELLON UNIVERSITY AND THE CONTRIBUTORS TO THIS WORK         ##
##  DISCLAIM ALL WARRANTIES WITH REGARD TO THIS SOFTWARE, INCLUDING      ##
##  ALL IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS, IN NO EVENT   ##
##  SHALL CARNEGIE MELLON UNIVERSITY NOR THE CONTRIBUTORS BE LIABLE      ##
##  FOR ANY SPECIAL, INDIRECT OR CONSEQUENTIAL DAMAGES OR ANY DAMAGES    ##
##  WHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN   ##
##  AN ACTION OF CONTRACT, NEGLIGENCE OR OTHER TORTIOUS ACTION,          ##
##  ARISING OUT OF OR IN CONNECTION WITH THE USE OR PERFORMANCE OF       ##
##  THIS SOFTWARE.                                                       ##
##                                                                       ##
###########################################################################
##                                                                       ##
##  An attempt at Hinton's dark knowledge using Random Forests           ##
##  (in all senses of that expression)                                   ##
##                                                                       ##
###########################################################################

# Run this in the voice directory

export SIODHEAPSIZE=1680000

CODE_DIR=/home/pmuthuku/code/Dark_trees
FV_VOICENAME=$1

cd rf_models/

mkdir -p dark_trees

# for i in trees_*
# do
    
#     echo "Predictions of $i"

#     cd $i
    
#     ls -1 *_?_mcep.tree | parallel -k $CODE_DIR/get_preds.sh

#     cd ..

# done


# Let's combine the feats and wagon predictions of all the trees into
# one big file per state
mkdir -p dark_feats
mkdir -p dark_preds

#This step and many of the steps below are intentionally left serial
#because of the amount of memory each process will use
for i in trees_01/*.feats #Using the list in the first tree dir
do

    fname=`basename $i .feats`
    echo "Combining Feats and Predictions for $fname"
    cat trees_??/$fname.feats > dark_feats/$fname.feats
    cat trees_??/$fname.wagon_encd |
    $ESTDIR/bin/ch_track -itype ascii -otype est_binary -s 0.005 -o dark_preds/$fname.preds

done


# # One tree to rule them all
ls -1 dark_feats/*.feats | parallel -k $CODE_DIR/build_the_one_tree.sh

cd ../festival/
cp -pr trees light_trees 

# Copy over the dark trees
cp -p ../rf_models/dark_trees/*.tree trees/

#Go back up to the voice directory and combine trees together
cd ../
$ESTDIR/../festival/bin/festival --heap $SIODHEAPSIZE -b festvox/${FV_VOICENAME}_cg.scm festvox/clustergen_build.scm '(begin
     (voice_'$FV_VOICENAME'_cg)
     (clustergen:collect_mcep_trees 
        '$FV_VOICENAME'::clustergen_f0_trees clustergen_params "mcep")
   )'


sed -i 's/cg:rfs 20/cg:rfs nil/' festvox/clustergen.scm

