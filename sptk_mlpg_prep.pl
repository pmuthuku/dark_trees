#!/bin/perl

use strict;
use warnings;

my $line;
my @numbers;
my $i;

while(<STDIN>){

    $line = $_;
    chomp($line);
    $line =~ s/^\s+|\s+$//g; #Removes leading and trailing white space that messes up the split function
    @numbers=split(/\s+/,$line);

    # Interleaving statics and deltas for SPTK
    for ($i=0; $i <= $#numbers; $i=$i+2){
	print "$numbers[$i] ";
    }
    
    for ($i=1; $i <= $#numbers; $i=$i+2){
	print "$numbers[$i] ";
    }
    
    print "\n";
}
